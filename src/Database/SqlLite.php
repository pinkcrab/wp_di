<?php

declare(strict_types=1);

/**
 * Sample database connection using SqlLite.
 */

namespace WP_DI\Example\Database;

use PDO;
use WP_DI\Example\Database\Database_Interface;

class SqlLite implements Database_Interface {

	/**
	 * PDO Connection
	 *
	 * @var PDO
	 */
	protected $connection;

	public function __construct( PDO $connection ) {
		$this->connection = $connection;
	}

}
