<?php

declare(strict_types=1);

/**
 * Sample database connection using SqlLite.
 */

namespace WP_DI\Example\Database;

use WPDB;

class WP_WPDB implements Database_Interface {

	protected $wpdb;

	public function __construct( WPDB $wpdb ) {
		$this->wpdb = $wpdb;
	}
}
