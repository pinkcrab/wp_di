<?php

declare(strict_types=1);

/**
 * This is a sample of the API connection.
 */

namespace WP_DI\Example\WooCommerce;

use WP_DI\Example\WooCommerce\Response;
use WP_DI\Example\Cache\Cache_Interface;

class API_Connection {

	protected const CACHE_EXPIRY = 1 * HOUR_IN_SECONDS;

	protected $cache;
	protected $response;


	public function __construct( Cache_Interface $cache, Response $response ) {
		$this->cache    = $cache;
		$this->response = $response;
	}

	/**
	 * To get live stock either form api or cached.
	 *
	 * @param string $sku
	 * @param string $location
	 * @param bool $use_cache
	 * @return string
	 */
	public function get_stock_level( string $sku, string $location, bool $use_cache = true ): string {

		// Get the current combinations hash.
		$cache_hash_key = $this->cache->generate_key_hash( $sku, $location );

		// Maybe from cache?
		$stock_level = $use_cache ? $this->cache->retrieve( $cache_hash_key ) : null;

		// If we have no cached value (or request not from cache.)
		if ( empty( $stock_level ) ) {
			$stock_level = $this->do_call(
				'stock',
				array(
					'sku'      => $sku,
					'location' => $location,
				)
			);

			$this->cache->store( $cache_hash_key, $stock_level, self::CACHE_EXPIRY );
		}

		return $stock_level ?
			$this->response->success_json( $stock_level ) :
			$this->response->empty_json();
	}

	/**
	 * This does the actual call.
	 *
	 * @param string $action
	 * @param array $data
	 * @return array
	 */
	private function do_call( string $action, array $data ): array {
		return array( 'some', 'data' );
	}
}
