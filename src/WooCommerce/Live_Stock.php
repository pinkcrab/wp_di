<?php

declare(strict_types=1);

/**
 * This is a sample live stock service.
 */

namespace WP_DI\Example\WooCommerce;

use WC_Product;

class Live_Stock {

	protected $api_connection;

	public function __construct( API_Connection $api_connection ) {
		$this->api_connection = $api_connection;
	}

	/**
	 * This attmepts to get from cached first.
	 * You could use this on product pages, where cached is fine.
	 *
	 * @param WC_Product $product
	 * @param string $location
	 * @return int
	 */
	public function get_cached_stock( WC_Product $product, string $location ): int {
		return $this->api_connection->get_stock_level( $product->get_sku(), $location );
	}

	/**
	 * This will just get the current value and update the cache.
	 * You could use this in the cart, to ensure it is still in stock.
	 *
	 * @param WC_Product $product
	 * @param string $location
	 * @return int
	 */
	public function get_current_stock( WC_Product $product, string $location ): int {
		return $this->api_connection->get_stock_level( $product->get_sku(), $location, false );
	}

	/**
	 * Show some content.
	 *
	 * @return void
	 */
	public function product_add_to_cart_template() {
		// This is where we would call get_cached_stock();
		print( 'current stock levels are .....' );
	}

	/**
	 * Used to verify if enough stock to complete the order.
	 *
	 * @param \WC_Product $product
	 * @param string $location
	 * @param int $stock
	 * @return bool
	 */
	public function verify_stock_for_order( WC_Cart $cart ): bool {
		// Pretend we have the customer chosen location in session.
		$location = WC()->session->get( 'delviery_location' );
		foreach ( $cart->get_items() as $cart_item ) {
			// If the stock the user has added is greater than current stock.
			if ( $this->get_current_stock( $cart_item->get_product(), $location ) < $cart_item->get_quantity() ) {
				// Adjust the users stock for order here and throw a notification or something.
			}
		}
	}
}
