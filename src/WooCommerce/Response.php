<?php

declare(strict_types=1);

/**
 * This is a sample of the API connection.
 */

namespace WP_DI\Example\WooCommerce;

class Response {

	public function success_json( ...$data ) {
		return wp_send_json( $data, 200 );
	}

	public function empty_json() {
		return wp_send_json( null, 204 );
	}
}
