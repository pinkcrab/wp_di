<?php

declare(strict_types=1);

/**
 * Does usery stuff with users
 */

namespace WP_DI\Example;

class User_Stuff {
	/**
	 * Its all a lie
	 *
	 * @var bool
	 */
	public $apparently_i_do_something = false;
}
