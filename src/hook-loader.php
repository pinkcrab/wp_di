<?php

declare(strict_types=1);

/**
 * Register all primary hook calls here.
 */

namespace WP_DI\Example;

use WP_DI\Core\App;

/**
 * Add our rules to dice which are needed from this side.
 */
App::dice()->addRules(
	array(

		/** DEFINE THE CONSTRUCTION PARMS */

		// The SqlLite class has this as a dependency.
		'PDO'                                 => array(
			'shared'          => true,
			'inherit'         => true,
			'constructParams' => array( 'sqlite:test.db' ),
		),
		// The WPDB database class has this as a dependency
		'WP_WPDB'                             => array(
			'shared'          => true,
			'inherit'         => true,
			'constructParams' => array( $GLOBALS['wpdb'] ),
		),



		/** HANDLING CONSTRUCTOR PROPS AS INTERFACES */

		// This allows more granular control if you use interfaces for you dependencies.
		// Becuase its shared, we get the same instance of Thingy when we make ANY thingy.
		// As inherit is true, any class which extends Thingy will extend the same instance.
		'WP_DI\Example\Thingy'                => array(
			'shared'        => true,
			'inherit'       => true,
			'substitutions' => array(
				'WP_DI\Example\Cache\Cache_Interface' => 'WP_DI\Example\Cache\Transient_Cache',
				// 'WP_DI\Example\Cache\Cache_Interface' => 'WP_DI\Example\Cache\File_Cache',
			),
		),
		// This isnt shared, so we will get new instances for both calls.
		// This will not in
		'WP_DI\Example\Something_Usery'       => array(
			'shared'        => true,
			'inherit'       => false,
			'substitutions' => array(
				'WP_DI\Example\Database\Database_Interface' => 'WP_DI\Example\Database\SqlLite',
				// 'WP_DI\Example\Database\Database_Interface' => 'WP_DI\Example\Database\WP_WPDB',
			),
		),



		/* ANOTHER EXAMPLE BUT WITH A GLOBAL RULE*/

		// You can either do as above and set the interface to use based on the class
		// being created
		/* 'WP_DI\Example\WooCommerce\API_Connection' => array(
			'shared'        => true,
			'inherit'       => true,
			'substitutions' => array(
				'WP_DI\Example\Cache\Cache_Interface' => 'WP_DI\Example\Cache\File_Cache',
			),
		), */

		// Or set a global rule for the interface
		// You can overrule this by setting rules for the class
		// For example if you comment out 'WP_DI\Example\Thingy' rule above, it will use File_Cache
		// not Transient_Cache for the WP_DI\Example\Cache\Cache_Interface.
		'WP_DI\Example\Cache\Cache_Interface' => array(
			'instanceOf' => 'WP_DI\Example\Cache\File_Cache',
			// 'constructParams' => array( $GLOBALS['wpdb'] ), // IF YOU NEED THEM!
		),
	)
);

// This is how you create an instance of your objects.
print( '<pre>' );
var_dump( App::make( 'WP_DI\Example\Thingy' ) );
var_dump( App::make( 'WP_DI\Example\Something_Usery' ) );
var_dump( App::make( 'WP_DI\Example\Combo' ) );
var_dump( App::make( 'WP_DI\Example\WooCommerce\Live_Stock' ) );
print( '</pre>' );

// Use in hook calls.
add_action( 'init', array( App::make( 'WP_DI\Example\Combo' ), 'something' ) );


// Its not ideal, but you can pass the current post and query in if you dont need
// to create an instance too early.
// $GLOBALS['wp_query'] can be found from init onwards and $GLOBALS['post'] on wp and onwards.
add_action(
	'wp',
	function() {
		App::dice()->addRules(
			array(
				'WP_Post'  => array(
					'shared'          => false, // DO NOT SHARE AS CAN BE IN A LOOP!
					'inherit'         => false, // DO NOT SHARE AS CAN BE IN A LOOP!
					'constructParams' => array( $GLOBALS['post'] ),
				),
				'WP_Query' => array(
					'shared'          => false, // DO NOT SHARE AS CAN BE IN A LOOP!
					'inherit'         => false, // DO NOT SHARE AS CAN BE IN A LOOP!
					'constructParams' => array( $GLOBALS['wp_query'] ),
				),
			)
		);

		// You have to call this here, else it will thorw warnings, if you call outside of this the callback.
		add_filter( 'the_post', array( App::make( 'WP_DI\Example\Loop_Helper' ), 'something' ) );
	}
);

// WOOCOMMERCE LIVE STOCK.
// Show on product page.
add_action(
	'woocommerce_after_add_to_cart_quantity',
	array( App::make( 'WP_DI\Example\WooCommerce\Live_Stock' ), 'product_add_to_cart_template' )
);
// Adjust stock levels in the cart if customer has more than current stocks.
add_action(
	'woocommerce_before_calculate_totals',
	array( App::make( 'WP_DI\Example\WooCommerce\Live_Stock' ), 'verify_stock_for_order' )
);
