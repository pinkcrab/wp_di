<?php

declare(strict_types=1);

/**
 * Can be used in a post as part of a loop.
 */

namespace WP_DI\Example;

use WP_Post, WP_Query;

class Loop_Helper {

	protected $post;
	protected $wp_query;

	/**
	 * These are all nullable get avoid being called to early and throwing errors.
	 *
	 * @param WP_Post|null $post
	 * @param WP_Query|null $wp_query
	 */
	public function __construct( ?WP_Post $post = null, ?WP_Query $wp_query = null ) {
		$this->post     = $post;
		$this->wp_query = $wp_query;
	}

	/**
	 * Filter the post object.
	 *
	 * @param WP_Post $post
	 * @return WP_Post
	 */
	public function something( WP_Post $post ): WP_Post {
		$post->custom_prop = 'DI is magic';
		var_dump( $post );
		return $post;
	}
}

