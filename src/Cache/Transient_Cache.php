<?php

declare(strict_types=1);

/**
 * Example wp transient based cache.
 */

namespace WP_DI\Example\Cache;

use WP_DI\Example\Cache\Cache_Interface;

class Transient_Cache implements Cache_Interface {

	public function generate_key_hash( ...$data ) {
		return md5( join( $data ) );
	}

    public function retrieve( string $hash ) {
		return 'SOMETHING FETCHED FROM TRANSIENTS';
	}

	public function store( string $key, $data, int $expiry ) {
		print( 'Stored ' . $key );
	}
}
