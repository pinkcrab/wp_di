<?php

declare(strict_types=1);

/**
 * This is the thingy, it does the thing with added cache
 */

namespace WP_DI\Example;

use WP_DI\Example\Database\Database_Interface;

class Something_Usery {

	protected $database;
	protected $user;

	public function __construct( Database_Interface $database, User_Stuff $user ) {
		$this->database = $database;
		$this->user     = $user;
	}
}
