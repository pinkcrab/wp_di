<?php

declare(strict_types=1);

/**
 * This is the thingy, it does the thing with added cache
 */

namespace WP_DI\Example;

use WP_DI\Example\Cache\Cache_Interface;

class Thingy {

	protected $cache;

	public function __construct( Cache_Interface $cache ) {
		$this->cache = $cache;
	}

	/**
	 * Do something.
	 *
	 * @return void
	 */
	public function action() {
		print( 'Look mum no hands!' );
		var_dump( $this->cache );
	}

}
