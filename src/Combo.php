<?php

declare(strict_types=1);

/**
 * If just using classes and not interfaces.
 * You do not need to define them on the config.
 */

namespace WP_DI\Example;

use WP_DI\Example\Thingy;
use WP_DI\Example\Something_Usery;

class Combo {

	protected $thingy;
	protected $something_usery;

	public function __construct( Thingy $thingy, Something_Usery $something_usery ) {
		$this->thingy          = $thingy;
		$this->something_usery = $something_usery;
	}

	public function something(): void {
		print( '<p style="font-size:36px; text-align: center">SOMETHING!!!!</p>' );
	}
}
