# A RAMBLING EXAMPLE OF DICE IN WP

Please ecxuse the comment and instructions, I am not really one for writing these things and dyslexia makes its less than enjoyable.

## So what is this?

Well it started as a throw away question on twitter and here i am doing an example of using Dice wtih WordPress.

The "plugin" is laid out in a way i wouldnt use this. Ideally i would have the app (core) loaded in mu-plugins with its own autoloader (no dependencies for usual wp + composer reasons). Then my src would either be part of a theme or a seperate plugin. The main thing is ensuring that core is loaded before the src is. 

## What is Dice?

Well its a bit like magic really, but to find out more visit the authors site here https://r.je/dice

## Why should you use it?

WordPress and modern php coding practises are not exactly 2 things you would put together. Well i might have when i started learning php back in the dark days of PHP4. So a lot of WP is based on globals which means we end up doing this a lot.

```php
// This make many very sad!
class Thing2{    
// ...
    public function __construct(){
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->service_a = new ServiceA();
        $this->service_b = new ServiceB('test');
    }
    // ...
}

// This allow us to do 
add_action( 'wp', [ new Thing2(), 'some_method' ] );
```
Obviously its much better to inject your dependecnies and uncouple as much as we can. So with DICE we can do the following.

```php
class Thing2 {

	public function __construct(
		WPDB $wpdb,
		ServiceA $service_a,
		ServiceB $service_b
	) {
		$this->wpdb      = $wpdb;
		$this->service_a = $service_a;
		$this->service_b = $service_b;
	}
}

// The dice rules would look like this
array(
    'ServiceB' => array(
        'constructParams' => array( 'test' ),
    // This will pass 'test' as a param to ServiceB(string $foo)
    ),
)

// This allow us to do
// We dont need to define ServiceA as Dice can handle instancing and it needs no prarams in the constructor.
add_action( 'wp', array( App::make( 'Thing2' ), 'some_method' ) );


// We can even use interfaces to uncouple the actual classes being used.
class Thing3 {
    
    // By using an interface, we can change to any class which implements it.
    public function __construct(
    WPDB $wpdb,
    Service_Interface $service_a,
    Service_Interface $service_b
    ) {
        $this->wpdb      = $wpdb;
        $this->service_a = $service_a;
        $this->service_b = $service_b;
    }
}

// The dice rules would look like this 
array(
    '$service_a' => array(
        'instanceOf' => 'ServiceA',
    ),
    '$service_b' => array(
        'instanceOf'      => 'ServiceB',
        'constructParams' => array( 'test' ), // If this was declared previously, it can be omitted.
    ),
)

// We could still use a global rule for the Service_Interface 
// But here this would have used ServiceB for both $service_a and $service_b.
/* array(
    'Service_Interface' => array(
        'instanceOf' => 'ServiceB',
    ),
) */
```