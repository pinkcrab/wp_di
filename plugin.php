<?php

declare(strict_types=1);

use WP_DI\Core\App;

/**
 * @wordpress-plugin
 * Plugin Name:     WordPress Using Dice DI Example
 * Plugin URI:      https://codeable.io/developers/glynn-quelch-pinkcrab/
 * Description:     Example of using Dice for DI in WordPress
 * Version:         1.0.0
 * Author:          Glynn Quelch
 * Author URI:      https://codeable.io/developers/glynn-quelch-pinkcrab/
 * License:         GPL-2.0+
 * License URI:     http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:     wp_di
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * If the autoload file exists, load.
 */
if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {

	// Load autoloader and call core bootloader.
	require_once __DIR__ . '/vendor/autoload.php';
	require_once __DIR__ . '/app/bootloader.php';

	// Load plugin action calls.
	require_once __DIR__ . '/src/hook-loader.php';

}

