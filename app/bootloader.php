<?php

declare(strict_types=1);

/**
 * The cores bootloader.
 *
 * Used to bind all services before init hook call.
 */

namespace WP_DI\Core;

use WP_DI\Core\Dice\Dice;
use WP_DI\Core\Dice\WP_Dice;

// Create an instace of the app.
$app = App::get_instance();

// Register all services.
$app->bind( 'dice', WP_Dice::constructWith( new Dice() ) );

// Define any rules needed for core or global.
// App::dice()->addRules(
// 	array(
// 		'Some\Class'      => array(
// 			'shared'          => true,
// 			'inherit'         => true,
// 			'constructParams' => array( 'foo' ),
// 		),
// 		'Someother\Class' => array(
// 			'substitutions' => array(
// 				'Some\Interace' => 'Class\To\UseForThisInterface',
// 			),
// 		),
// 	)
// );



