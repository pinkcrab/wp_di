<?php

declare(strict_types=1);

/**
 * A wrapper for Dice.
 *
 * It returns a new instance of it self when new rules are added.
 * So it just overwrites the current instance.
 */

namespace WP_DI\Core\Dice;

use WP_DI\Core\Dice\Dice;

class WP_Dice {

	protected $dice;

	/**
	 * Passes in the inital dice instance.
	 *
	 * @param \WP_DI\Core\Dice\Dice $dice
	 */
	public function __construct( Dice $dice ) {
		$this->dice = $dice;
	}

	/**
	 * Lazy stack instancing.
	 *
	 * @param \WP_DI\Core\Dice\Dice $dice
	 * @return self
	 */
	public static function constructWith( Dice $dice ): self {
		return new self( $dice );
	}

	/**
	 * Proxy for addRule.
	 *
	 * @param string $name
	 * @param array $rule
	 * @return self
	 */
	public function addRule( string $name, array $rule ): self {
		$this->dice = $this->dice->addRule( $name, $rule );
		return $this;
	}

	/**
	 * Proxy for addRules
	 *
	 * @param array $rules
	 * @return self
	 */
	public function addRules( array $rules ): self {
		$this->dice = $this->dice->addRules( $rules );
		return $this;
	}

	/**
	 * Proxy for create, but with third param removed (see dice code comments)
	 *
	 * @param string $name
	 * @param array $args
	 * @return object|null
	 */
	public function create( string $name, array $args ):? object {
		return $this->dice->create( $name, $args );
	}
}
