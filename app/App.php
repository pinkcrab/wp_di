<?php

declare(strict_types=1);

/**
 * The App container.
 */

namespace WP_DI\Core;

use Exception;

/**
 * Acts as the main app container.
 */

final class App {

	/**
	 * Holds the instance of the App container.
	 *
	 * @var \WP_DI\Core\App
	 */
	private static $instance;

	/**
	 * Holds binded data.
	 *
	 * @var array
	 */
	private $services = array();

	/**
	 * Create an instance of the App container.
	 */
	protected function __construct() {
	}

	/**
	 * Prevent cloning.
	 *
	 * @return void
	 */
	protected function __clone() {
	}

	/**
	 * Unserialise the app container.
	 *
	 * @throws Exception
	 * @return void
	 */
	public function __wakeup() {
		throw new Exception( 'You shouldnt really be able serialise me, so no idea why youre trying to deserialise me. Sorry but no!' );
	}

	/**
	 * Get active instance of App wrapper.
	 *
	 * @return self
	 */
	public static function get_instance(): self {
		return self::$instance ?? self::$instance = new static();
	}

	/**
	 * Binds data to the apps service container.
	 *
	 * @param string $key
	 * @param mixed $data
	 * @return void
	 */
	public function bind( string $key, $data ): void {
		$this->services[ $key ] = $data;
	}

	/**
	 * Retrives data from the service container.
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function call( string $key ) {
		return $this->services[ $key ] ?? null;
	}

	/**
	 * Retrives data from the service container.
	 *
	 * @param string $method
	 * @param array $args
	 * @return mixed
	 */
	public static function __callStatic( string $method, array $args ) {
		return self::$instance->services[ $method ] ?? null;
	}

	/**
	 * Creates an instance using Dice.
	 *
	 * @param string $class
	 * @param array $args
	 * @return object|null
	 */
	public static function make( string $class, array $args = array() ) {
		return array_key_exists( 'dice', self::$instance->services ) ?
			self::$instance->services['dice']->create( $class, $args ) :
			null;
	}
}
